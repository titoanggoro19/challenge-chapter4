class Car {
  static list = [];

  static init(cars) {
    this.list = cars.map((i) => new this(i));
  }

  constructor({
    id,
    plate,
    manufacture,
    model,
    image,
    rentPerDay,
    capacity,
    description,
    transmission,
    available,
    type,
    year,
    options,
    specs,
    availableAt,
  }) {
    this.id = id;
    this.plate = plate;
    this.manufacture = manufacture;
    this.model = model;
    this.image = image;
    this.rentPerDay = rentPerDay;
    this.capacity = capacity;
    this.description = description;
    this.transmission = transmission;
    this.available = available;
    this.type = type;
    this.year = year;
    this.options = options;
    this.specs = specs;
    this.availableAt = availableAt;
  }

  render() {
    return `
          <div class="carContainer align-items-stretch">
            <div class="card p-3 shadow p-3 mb-5 bg-body rounded">
              <div class="image-card">
                <img  src="${this.image}" class="w-100" alt="" style="max-height: 180px;object-fit: cover">
              </div>
              <div>
                <p class="fw-bold mt-3">${this.model}</p>
              </div>
              <div>
                <h5 class="fw-bolder">Rp. ${this.rentPerDay} / hari</h5>
              </div>
              <div class="vector-size">
              <div>
                <p class ="title">${this.description} </p>            
              </div>
              <div>
                  <p>
                    <img
                      src="../Images/vector_users.png"
                      class="me-4"
                      alt=""
                    />${this.capacity} orang
                  </p>
              </div>
              <div>
                  <p>
                    <img
                      src="../Images/vector_settings.png"
                      class="me-4"
                      alt=""
                    />${this.transmission}
                  </p>
              </div>
              <div>
                  <p>
                    <img
                      src="../Images/vector_calendar.png"
                      class="me-4"
                      alt=""
                      />Tahun ${this.year}
                  </p>
              </div>
              </div>
              <button class="btn btn-success btn-green-custom"> Pilih Mobil</button>
            </div>
          </div>
    `;
  }
}
// <p>id: <b>${this.id}</b></p>
// <p>plate: <b>${this.plate}</b></p>
// <p>manufacture: <b>${this.manufacture}</b></p>
// <p>model: <b>${this.model}</b></p>
// <p>available at: <b>${this.availableAt}</b></p>
// <img src="${this.image}" alt="${this.manufacture}" width="64px"></img>
